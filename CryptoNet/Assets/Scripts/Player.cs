﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Variables

    //Player
    public float maxHealth;
    public float health;

    public float movementSpeed;
    Animation anim;
    public float attackTimer;
    private float currentAttackTimer;

    //PMR
    public GameObject playerMovePoint;
    private Transform pmr;

    private bool moving;
    private bool triggerPMR;

    //Enemy
    private bool triggerEnemy;
    public GameObject _enemy;
    private bool attacking;
    private bool followingEnemy;

    private float damage;
    public float minDamage;
    public float maxDamage;
    private bool attacked;




    private void Start()
    {
        pmr = Instantiate(playerMovePoint.transform, this.transform.position, Quaternion.identity);
        pmr.GetComponent<BoxCollider>().enabled = false;
        anim = GetComponent<Animation>();
        currentAttackTimer = attackTimer;
        _enemy = GameObject.FindWithTag("Enemy");
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        float hitDistance = 0.0f;

        if (playerPlane.Raycast(ray, out hitDistance))
        {
            Vector3 mousePosition = ray.GetPoint(hitDistance);
            //Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            if (Input.GetMouseButtonDown(0))
            {
                moving = true;
                triggerPMR = false;
                pmr.transform.position = mousePosition;
                pmr.GetComponent<BoxCollider>().enabled = true;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.tag == "Enemy")
                    {
                        _enemy = hit.collider.gameObject;
                        followingEnemy = true;

                    }
                    else
                    {
                        //_enemy = null;
                        followingEnemy = false;
                    }
                }

                //_enemy = null;
                //followingEnemy = false;
            }
        }

        if (moving) { Move(); }
        else
        {
            if (attacking)
            {
                Attack();
            }
            else
            {
                Idle();
            }
        }

        if (triggerPMR) { moving = false; }

        if (triggerEnemy) { Attack(); }

        if (attacked)
        {
            currentAttackTimer -= 1 * Time.deltaTime;
        }

        if (currentAttackTimer <= 0)
        {
            currentAttackTimer = attackTimer;
            attacked = false;
        }
    }


    public void Attack()
    {
        if (!attacked)
        {
            damage = Random.Range(minDamage, maxDamage);
            print(damage);


            attacked = true;
        }

        if (_enemy)
        {
            transform.LookAt(_enemy.transform);
            _enemy.GetComponent<Enemy>().aggro = true;
        }
        anim.CrossFade("attack");

    }


    public void Idle()
    {
        anim.CrossFade("idle");
    }

    public void Move()
    {

        if (followingEnemy)
        {
            if (!triggerEnemy)
            {
                transform.position = Vector3.MoveTowards(transform.position, _enemy.transform.position, movementSpeed);
                this.transform.LookAt(_enemy.transform);
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, pmr.transform.position, movementSpeed);
            this.transform.LookAt(pmr.transform);
        }
        anim.CrossFade("walk");
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PMR") { triggerPMR = true; }
        if (other.tag == "Enemy") { triggerEnemy = true; }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "PMR") { triggerPMR = false; }
        if (other.tag == "Enemy") { triggerEnemy = false; }
    }
}