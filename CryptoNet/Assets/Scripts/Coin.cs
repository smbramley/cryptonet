﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField]
    private AudioClip _coinPickup;

    UIManager uiManager;

    private void Start()
    {
        uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        //_coinPickup = GetComponent<AudioClip>();
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayerFPS player = other.GetComponent<PlayerFPS>();
                if (player != null&&_coinPickup!=null)
                {
                    player.hasCoin = true;
                    AudioSource.PlayClipAtPoint(_coinPickup, transform.position, 1f);
                    if (uiManager != null) { uiManager.CollectedCoin(); }
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
