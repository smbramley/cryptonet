﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkShop : MonoBehaviour
{

    UIManager uiManager;

    private void Start()
    {
        uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlayerFPS player = other.GetComponent<PlayerFPS>();
                if (player != null)
                {
                    player.hasCoin = false;
                    if (uiManager != null)
                    {
                        uiManager.RemoveCoin();
                    }
                }
            }
        }
    }
}
